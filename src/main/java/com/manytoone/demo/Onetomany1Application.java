package com.manytoone.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class Onetomany1Application {

	public static void main(String[] args) {
		SpringApplication.run(Onetomany1Application.class, args);
	}

}
