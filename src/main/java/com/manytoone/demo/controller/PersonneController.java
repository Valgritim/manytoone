package com.manytoone.demo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.manytoone.demo.exceptions.ResourceNotFoundException;
import com.manytoone.demo.model.Personne;
import com.manytoone.demo.repository.PersonneRepository;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/api/v1")
public class PersonneController {
    @Autowired
    private PersonneRepository PersonneRepository;
    
    @GetMapping("/personnes")
    public List <Personne> getPersonnes() {
        return PersonneRepository.findAll();
    }
    
    @GetMapping("/personnes/{id}")
    public ResponseEntity < Personne > getPersonneById(
        @PathVariable(value = "id") Long personneId) throws ResourceNotFoundException {
        Personne personne = PersonneRepository.findById(personneId)
            .orElseThrow(() -> new ResourceNotFoundException("Personne not found :: " + personneId));
        return ResponseEntity.ok().body(personne);
    }
    
    @PostMapping("/personnes")
    public Personne createPersonne(@Valid @RequestBody Personne personne) {
        return PersonneRepository.save(personne);
    }
    
    @PutMapping("/personnes/{id}")
    public ResponseEntity < Personne > updatePersonne(
        @PathVariable(value = "id") Long personneId,
        @Valid @RequestBody Personne personneDetails) throws ResourceNotFoundException {
        Personne personne = PersonneRepository.findById(personneId)
            .orElseThrow(() -> new ResourceNotFoundException("Personne not found :: " + personneId));
        personne.setFirstName(personneDetails.getFirstName());
        personne.setLastName(personneDetails.getLastName());
        personne.setAge(personneDetails.getAge());
        final Personne updatedPersonne = PersonneRepository.save(personne);
        return ResponseEntity.ok(updatedPersonne);
    }
    
    @DeleteMapping("/personnes/{id}")
    public Map <String, Boolean> deletePersonne(
        @PathVariable(value = "id") Long personneId) throws ResourceNotFoundException {
        Personne personne = PersonneRepository.findById(personneId)
            .orElseThrow(() -> new ResourceNotFoundException("Personne not found :: " + personneId));
        PersonneRepository.delete(personne);
        Map <String, Boolean> response = new HashMap <> ();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
