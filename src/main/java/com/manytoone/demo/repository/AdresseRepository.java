package com.manytoone.demo.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.manytoone.demo.model.Adresse;

@Repository
public interface AdresseRepository extends JpaRepository<Adresse, Long>{
	 List<Adresse> findByPersonneId(Long instructorId);
	 Optional<Adresse> findByIdAndPersonneId(Long id, Long instructorId);
}
