package com.manytoone.demo.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.manytoone.demo.model.Personne;

@Repository
public interface PersonneRepository extends JpaRepository<Personne, Long>{	
	
}
