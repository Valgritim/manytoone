package com.manytoone.demo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.manytoone.demo.model.Adresse;
import com.manytoone.demo.model.Personne;
import com.manytoone.demo.repository.AdresseRepository;


@RunWith(SpringRunner.class)
@DataJpaTest
class AdresseControllerTest {

	@Autowired
	private AdresseRepository adresseRepository;
	@Autowired
	private TestEntityManager entityManager;
	
	@Test
	void testGetAdressesByPersonne() {
		Personne personne1 = new Personne("Younes","Temar","27");
		Adresse adresse1 = new Adresse("ecureuils","06210","Nice",personne1);
		Adresse adresse2 = new Adresse("ecureuils","06210","Nice",personne1);
		entityManager.persist(adresse1);
		entityManager.persist(adresse2);
		List<Adresse> getFromDb = adresseRepository.findByPersonneId(personne1.getId());
		assertThat(getFromDb.size()).isEqualTo(2);
	}

	@Test
	void testSave() {
		Personne personne1 = new Personne("Younes","Temar","27");
		Adresse adresse1 = new Adresse("ecureuils","06210","Nice",personne1);
		Adresse adresse2 = new Adresse("ecureuils","06210","Nice",personne1);
		entityManager.persist(adresse1);
		entityManager.persist(adresse2);
		List<Adresse> getFromDb = adresseRepository.findAll();
		assertThat(getFromDb.size()).isEqualTo(2);
	}

	@Test
	void testUpdateAdresse() {
		Personne personne1 = new Personne("Younes","Temar","27");
		Adresse adresse1 = new Adresse("ecureuils","06210","Nice",personne1);
		entityManager.persist(adresse1);
		Adresse getInDb = adresseRepository.getOne(adresse1.getId());
		getInDb.setCodePostal("06200");
		entityManager.persist(getInDb);
		Adresse getInDbNew = adresseRepository.getOne(adresse1.getId());
		assertThat(getInDbNew.getCodePostal()).isEqualTo("06200");
	}

	@Test
	void testDeleteAdresse() {
		Personne personne1 = new Personne("Younes","Temar","27");
		Adresse adresse1 = new Adresse("ecureuils","06210","Nice",personne1);
		Adresse adresse2 = new Adresse("ecureuils","06210","Nice",personne1);
		entityManager.persist(adresse1);
		entityManager.persist(adresse2);
		entityManager.remove(adresse2);
		List<Adresse> adresses = adresseRepository.findAll();
		assertThat(adresses.size()).isEqualTo(1);
	}

}
