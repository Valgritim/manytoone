package com.manytoone.demo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.manytoone.demo.model.Personne;
import com.manytoone.demo.repository.PersonneRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
class PersonneControllerTest {

	@Autowired
	private PersonneRepository personneRepository;
	@Autowired
	private TestEntityManager entityManager;
	
	@Test
	void testGetPersonnes() {
		Personne personne1 = new Personne("Younes","Temar","27");
		Personne personne2 = new Personne("Ahmed","Temar","1");
		entityManager.persist(personne1);
		entityManager.persist(personne2);
		List<Personne> personnesFromDb = personneRepository.findAll();
		assertThat(personnesFromDb.size()).isEqualTo(2);
		
	}

	@Test
	void testGetPersonneById() {
		Personne personne1 = new Personne("Younes","Temar","27");
		Personne persist = entityManager.persist(personne1);
		Personne getFromDb = personneRepository.getOne(persist.getId());
		assertEquals(persist,getFromDb);
		assertThat(getFromDb.equals(persist));
	}

	@Test
	void testCreatePersonne() {
		Personne personne1 = new Personne("Younes","Temar","27");
		Personne persist = entityManager.persist(personne1);
		Personne getFromDb = personneRepository.getOne(persist.getId());
		assertEquals(persist,getFromDb);
		assertThat(getFromDb.equals(persist));
	}

	@Test
	void testUpdatePersonne() {
		Personne personne1 = new Personne("Younes","Temar","27");
		Personne persist = entityManager.persist(personne1);
		Personne getFromDb = personneRepository.getOne(persist.getId());
		getFromDb.setFirstName("youyou");
		entityManager.persist(getFromDb);
		Personne getFromDbNew = personneRepository.getOne(getFromDb.getId());
		assertThat(getFromDbNew.getFirstName()).isEqualTo("youyou");
		
		
	}

	@Test
	void testDeletePersonne() {
		Personne personne1 = new Personne("Younes","Temar","27");
		Personne personne2 = new Personne("Yasmina","Temar","22");
		Personne persist = entityManager.persist(personne1);
		entityManager.persist(personne2);
		entityManager.remove(personne2);
		List<Personne> personnesFromDb = personneRepository.findAll();
		assertThat(personnesFromDb.size()).isEqualTo(1);
		
	}

}
